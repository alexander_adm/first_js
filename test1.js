var global_utm = (localStorage.getItem('global_utm')) ? localStorage.getItem('global_utm'): '';

if(utm.includes('cmc_target_korporativ') || (localStorage.getItem('numreplace_sms_fbcorp') && global_utm.includes('cmc_target_korporativ') )){
	if (url.includes('fortboyard/corporate')){
		localStorage.setItem('numreplace_sms_fbcorp-status', false);
		setPhone("numreplace_sms_fbcorp");
	}
}

if(utm.includes('cmc_target_adult') || (localStorage.getItem('numreplace_sms_fbadult') && global_utm.includes('cmc_target_adult') )){
	if (url.includes('fortboyard/birthday')){
		setPhone("numreplace_sms_fbadult");
	}
}

if(utm.includes('cmc_target_child_dr') || (localStorage.getItem('numreplace_sms_fbchild') && global_utm.includes('cmc_target_child_dr') )){
	if (url.includes('fortboyard/child-birthday')){
		setPhone("numreplace_sms_fbchild");
	}
}

if(utm.includes('cmc_target_korporativ') || (localStorage.getItem('numreplace_sms_zlcorp') && global_utm.includes('cmc_target_korporativ') )){
	if (url.includes('goldrush/corporate')){
		setPhone("numreplace_sms_zlcorp");
	}
}

if(utm.includes('cmc_target_adult') || (localStorage.getItem('numreplace_sms_zladult') && global_utm.includes('cmc_target_adult') )){
	if (url.includes('goldrush/birthday')){
		setPhone("numreplace_sms_zladult");
	}
}

if(utm.includes('cmc_target_child_dr') || (localStorage.getItem('numreplace_sms_zlchild') && global_utm.includes('cmc_target_child_dr') )){
	if (url.includes('goldrush/child-birthday')){
		setPhone("numreplace_sms_zlchild");
	}
}

if(utm.includes('possev_vk_new_site') || (localStorage.getItem('numreplace_vk_fb') && global_utm.includes('possev_vk_new_site') )){
	if (url.includes('fortboyard')){
		setPhone("numreplace_vk_fb");
	}
}

if(utm.includes('possev_vk_new_site') || (localStorage.getItem('numreplace_vk_zl') && global_utm.includes('possev_vk_new_site') )){
	if (url.includes('goldrush')){
		setPhone("numreplace_vk_zl");
	}
}

if(utm.includes('possev_vk_new_site') || (localStorage.getItem('numreplace_vk_jm') && global_utm.includes('possev_vk_new_site') )){
	if (url.includes('jumanji')){
		setPhone("numreplace_vk_jm");
	}
}

function setPhone(item){
	let numbers = document.querySelectorAll('.phonenumber');
	Array.prototype.forEach.call(function(el, i){
		el.textContent = localStorage.getItem(item);
		el.setAttribute('href', 'tel:' + localStorage.getItem(item));
	});
}