const expirationDuration = 1000 * 60 * 60 * 72;
const prevAccepted = localStorage["accepted"];
const currentTime = new Date().getTime();

var utm = window.location.search;
var prevUtm = localStorage["global_utm"];
var prevAcceptedExpired = prevAccepted != undefined && currentTime - prevAccepted > expirationDuration;

if(utm){
	localStorage["accepted"] = currentTime;
	localStorage["global_utm"] = utm;
	setInput(utm);
	console.log('set utm');
} else if (prevUtm && prevAcceptedExpired && utm === "") {
	delete localStorage["accepted"];
	delete localStorage["global_utm"];
	console.log('remove utm');
} else {
	if(prevUtm){
		setInput();
		console.log('load utm');
	}
}
	
function setInput(value = prevUtm){
	let inputs = document.querySelectorAll('form input');
	var matches = function(el, selector) {
		return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
	};
	
	Array.prototype.forEach.call(inputs, function(el, i){
		if (matches(el, "[name='utm_source']")){
			el.setAttribute('value', value);
		}
	});
}