var global_utm = (localStorage.getItem('global_utm')) ? localStorage.getItem('global_utm'): '';

let arrs = [
	{
		inc: "cmc_target_korporativ",
		local: "numreplace_sms_fbcorp",
		url: "fortboyard/corporate"
	},
	{
		inc: "cmc_target_adult",
		local: "numreplace_sms_fbadult",
		url: "fortboyard/birthday"
	},
	{
		inc: "cmc_target_child_dr",
		local: "numreplace_sms_fbchild",
		url: "fortboyard/child-birthday"
	},
	{
		inc: "cmc_target_korporativ",
		local: "numreplace_sms_zlcorp",
		url: "goldrush/corporate"
	},
	{
		inc: "cmc_target_adult",
		local: "numreplace_sms_zladult",
		url: "goldrush/birthday"
	},
	{
		inc: "cmc_target_child_dr",
		local: "numreplace_sms_zlchild",
		url: "goldrush/child-birthday"
	},
	{
		inc: "possev_vk_new_site",
		local: "numreplace_vk_fb",
		url: "fortboyard"
	},
	{
		inc: "possev_vk_new_site",
		local: "numreplace_vk_zl",
		url: "goldrush"
	},
	{
		inc: "possev_vk_new_site",
		local: "numreplace_vk_jm",
		url: "jumanji"
	}
];


for(arr in arrs){
	if(utm.includes(arrs[arr].inc) || (localStorage.getItem(arrs[arr].local) && global_utm.includes(arrs[arr].inc) )){
		if (url.includes(arrs[arr].url)){
			if(arrs[arr].url == 'fortboyard/corporate')
				localStorage.setItem('numreplace_sms_fbcorp-status', false);
			setPhone(arrs[arr].local);
		}
	}
}

function setPhone(item){
	let numbers = document.querySelectorAll('.phonenumber');
	Array.prototype.forEach.call(function(el, i){
		el.textContent = localStorage.getItem(item);
		el.setAttribute('href', 'tel:' + localStorage.getItem(item));
	});
}